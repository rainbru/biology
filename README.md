# biology

A study to train a neural network to handle Non-Playable Characters.

# Dependencies

On Debian distributions :

	sudo apt install libwxbase3.0-dev libwxgtk3.0-dev libfann-dev

You also need to install the `libgtest-dev` package to run unit tests.

On arch-based ones :

	sudo pacman -Sy wxwidgets-gtk3

but fann in only available in the [AUR](https://aur.archlinux.org/packages/fann)
.

	paman build fann

# Building

	mkdir build
	cd build
	cmake ..
	make
	make check

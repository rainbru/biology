/* 
 * biology - A study to train a neural network to handle Non-Playable Characters
 *
 * Copyright (C) 2010, 2015-2018 Jérôme Pasquier
 *
 * biology is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * biology is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with biology.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _TIME_LINE_H_
#define _TIME_LINE_H_

// For compilers that support precompilation, includes "wx/wx.h".
#include <wx/wxprec.h>
#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

#include <string>

/** A simple frame that shows the elapsed game time
  *
  */
class TimeLine: public wxPanel
{
 public:
  TimeLine(wxFrame *parent, int id,const wxPoint &pos, const wxSize &size );
  virtual ~TimeLine();

  wxString gameDateTime();
  
 protected:
  void OnSize(wxSizeEvent&);
  void OnPaint(wxPaintEvent&);  

  void OnTimerTimeout(wxTimerEvent&);
  void OnRefresh(wxTimerEvent&);

  wxString gameDate();
  wxString gameTime();

  /// declare a static event table for that class
  wxDECLARE_EVENT_TABLE();

  
 private:
  wxFrame*    m_parent;
  wxTimer*    m_timer;    // The date/time addition timer
  wxTimer*    m_timer_refresh;
  wxDateTime* m_time;
  wxTimeSpan* m_incr_time; // Time incrementor
  wxDateSpan* m_incr_date; // Date incrementor


  int timeout;
};

#endif // _TIME_LINE_H_

/* 
 * biology - A study to train a neural network to handle Non-Playable Characters
 *
 * Copyright (C) 2010, 2015-2018 Jérôme Pasquier
 *
 * biology is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * biology is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with biology.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "main_window.h"

#include "config.h"
#include "time_line.h"
#include "log_pane.h"
#include "properties_pane.h"
#include "nn_configuration.h"

#include "libbiology/neural_network.h"

#include <iostream>
using namespace std;

enum
{
  ID_Hello = 1,
  ID_NNCONF = 2
};

wxBEGIN_EVENT_TABLE(MainWindow, wxFrame)
    EVT_MENU(ID_Hello,   MainWindow::OnHello)
    EVT_MENU(ID_NNCONF, MainWindow::OnNnConfiguration)
    EVT_MENU(wxID_EXIT,  MainWindow::OnExit)
    EVT_MENU(wxID_ABOUT, MainWindow::OnAbout)
wxEND_EVENT_TABLE()

MainWindow::MainWindow(NeuralNetwork* nw,
		       const wxString& title, const wxPoint& pos,
		       const wxSize& size)
           : wxFrame(NULL, wxID_ANY, title, pos, size)
{
  m_neuralNetwork = nw;

  SetMinSize(wxSize(800,600));
  // notify wxAUI which frame to use
  m_mgr.SetManagedWindow(this);
  
    wxMenu *menuFile = new wxMenu;
    menuFile->Append(ID_Hello, "&Hello...\tCtrl-H",
                     "Help string shown in status bar for this menu item");
    menuFile->AppendSeparator();
    menuFile->Append(wxID_EXIT);
    wxMenu *menuConf = new wxMenu;
    menuConf->Append(ID_NNCONF, "&Neural network");
    wxMenu *menuHelp = new wxMenu;
    menuHelp->Append(wxID_ABOUT);
    wxMenuBar *menuBar = new wxMenuBar;
    menuBar->Append( menuFile, "&File" );
    menuBar->Append( menuConf, "&Configuration" );
    menuBar->Append( menuHelp, "&Help" );
    SetMenuBar( menuBar );
    CreateStatusBar();
    SetStatusText( "Welcome to " PACKAGE " v" PACKAGE_VERSION);

    m_mgr.SetDockSizeConstraint(0.5, 0.3);
    
    TimeLine* tl = new TimeLine(this, wxID_ANY, wxDefaultPosition,
				wxSize(200,150));
    m_mgr.AddPane(tl, wxRIGHT, wxT("TimeLine"));


 
    m_log = new LogPane(tl, this, wxID_ANY,
			      wxDefaultPosition, wxSize(200,150));
    m_mgr.AddPane(m_log, wxBOTTOM, wxT("Log pane"));

    
    wxTextCtrl* text3 = new wxTextCtrl(this, -1, _("Main content window"),
				       wxDefaultPosition, wxSize(200,150),
				       wxNO_BORDER | wxTE_MULTILINE);

    m_props = new PropertiesPane(this, -1);

    // add the panes to the manager
    m_mgr.AddPane(m_props, wxLEFT, wxT("Simultation properties"));
    m_mgr.AddPane(text3, wxCENTER);
    
    // tell the manager to "commit" all the changes just made
    m_mgr.Update();
}

MainWindow::~MainWindow()
{
  // deinitialize the frame manager
  m_mgr.UnInit();

  m_log = NULL;
}


void MainWindow::OnExit(wxCommandEvent& event)
{
    Close( true );
}
void MainWindow::OnAbout(wxCommandEvent& event)
{
  //    wxMessageBox( "This is a wxWidgets' Hello world sample",
  //              "About Hello World", wxOK | wxICON_INFORMATION );
    m_log->log (LD_SIM, "Sending hello world message, testing scrolling...");
}
void MainWindow::OnHello(wxCommandEvent& event)
{
    wxLogMessage("Hello world from wxWidgets!");
}

void MainWindow::OnNnConfiguration(wxCommandEvent& event)
{
  NnConfigurationDialog dlg(wxT("Neutral network configuration"),
				m_neuralNetwork);
    if (dlg.ShowModal() == 5100)
      {
	cout << "NN modified" << endl;
      }
    else
      {
	cout << "NN NOT modified" << endl;
      }
}

/* 
 * biology - A study to train a neural network to handle Non-Playable Characters
 *
 * Copyright (C) 2010, 2015-2018 Jérôme Pasquier
 *
 * biology is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * biology is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with biology.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "nn_configuration.h"

#include <wx/spinctrl.h>

#include <iostream>

using namespace std;

enum
{
  LAYERS_SPIN = 1
};

wxBEGIN_EVENT_TABLE(NnConfigurationDialog, wxWindow)
    EVT_SPINCTRL(LAYERS_SPIN, NnConfigurationDialog::OnLayersNumberChange)
    EVT_BUTTON(wxID_CANCEL,   NnConfigurationDialog::OnCancel)
    EVT_BUTTON(wxID_OK,       NnConfigurationDialog::OnOk)
wxEND_EVENT_TABLE()

NnConfigurationDialog::NnConfigurationDialog(const wxString & title,
					      NeuralNetwork* nn):
wxDialog(NULL, -1, title, wxDefaultPosition, wxSize(450, 300)),
  layoutNumber(3),
  m_neuralNetwork(nn)
{

  // Add a label 
  wxStaticText *helpText = new wxStaticText(this, -1,
      wxT("Neural network configuration dialog."), 
      wxPoint(5, 5), wxSize(240, 20));

  wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);
  wxBoxSizer *hbox = new wxBoxSizer(wxHORIZONTAL);

  wxPanel *panel = new wxPanel(this, -1);
  new wxStaticBox(panel, -1, wxT("Layers"), 
				    wxPoint(5, 5), wxSize(440, 170));
  new wxStaticText(panel, -1, "# of layers :",
					       wxPoint(15, 35), wxDefaultSize);
    
  wxSpinCtrl* spin =  new wxSpinCtrl (panel, LAYERS_SPIN, "layers",
				      wxPoint(100, 30), wxSize(170, 30));
  spin->SetRange(1, 10);
  
  grid1 = new wxGrid(panel, -1, wxPoint(15, 65), wxSize(420,260));
  grid1->CreateGrid(layoutNumber,2); // rows, cols
  grid1->SetColFormatNumber(1);
  grid1->HideColLabels();
  
  wxButton *okButton = new wxButton(this, wxID_OK, wxT("Ok"), 
      wxDefaultPosition, wxSize(70, 30));
  wxButton *closeButton = new wxButton(this, wxID_CANCEL, wxT("Close"), 
      wxDefaultPosition, wxSize(70, 30));

  hbox->Add(okButton, 1);
  hbox->Add(closeButton, 1, wxLEFT, 5);

  vbox->Add(helpText, 0);
  vbox->Add(panel, 1);
  vbox->Add(hbox, 0, wxALIGN_CENTER | wxTOP | wxBOTTOM, 10);

  SetSizer(vbox);

  Centre();
}

void
NnConfigurationDialog::OnLayersNumberChange(wxSpinEvent& event)
{
  int l = event.GetPosition();
  cout << "Layers number changed : " << l << endl;

  if (l > layoutNumber)
    {
      grid1->AppendRows(l-layoutNumber);
    }
  else
    {
      int diff = layoutNumber - l;
      grid1->DeleteRows(layoutNumber - diff, diff);
     
    }
  
  layoutNumber = l;
}

void
NnConfigurationDialog::OnCancel(wxCommandEvent& event)
{
  EndModal(wxID_CANCEL);
}

void
NnConfigurationDialog::OnOk(wxCommandEvent& event)
{
  EndModal(wxID_OK);
}

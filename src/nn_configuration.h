/* 
 * biology - A study to train a neural network to handle Non-Playable Characters
 *
 * Copyright (C) 2010, 2015-2018 Jérôme Pasquier
 *
 * biology is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * biology is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with biology.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _NN_CONF_H_
#define _NN_CONF_H_

// For compilers that support precompilation, includes "wx/wx.h".
#include <wx/wxprec.h>
#ifndef WX_PRECOMP
    #include <wx/wx.h>
    #include <wx/grid.h>
#endif

#include "neural_network.h"

/** A configuration dialog for the neural network
  *
  * Show it using :
  * int i = dlg.ShowModal();
  *
  * If Ok is clicked, return value of ShowModal is 5100.
  */
class NnConfigurationDialog : public wxDialog
{
 public:
  NnConfigurationDialog(const wxString&, NeuralNetwork*);

 private:  
  void OnLayersNumberChange(wxSpinEvent& event);
  void OnCancel(wxCommandEvent& event);
  void OnOk(wxCommandEvent& event);
  wxDECLARE_EVENT_TABLE();

  wxGrid* grid1;           //!< The layers configuration grid
  int layoutNumber;
  NeuralNetwork* m_neuralNetwork;
};

#endif // !_NN_CONF_H_

/* 
 * biology - A study to train a neural network to handle Non-Playable Characters
 *
 * Copyright (C) 2010, 2015-2018 Jérôme Pasquier
 *
 * biology is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * biology is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with biology.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "properties_pane.h"

#include <iostream>
using namespace std;

PropertiesPane::PropertiesPane(wxWindow* parent, wxWindowID id):
  wxPropertyGrid(parent, id, wxDefaultPosition, wxSize(450,300),
		 wxPG_AUTO_SORT | wxPG_SPLITTER_AUTO_CENTER |wxPG_DEFAULT_STYLE )
{
  SetMinSize(wxSize(450,300));
  SetExtraStyle( wxPG_EX_HELP_AS_TOOLTIPS );
  Append( new wxStringProperty("Label", "Name", "Initial Value") );

  wxPropertyGrid* pg = this;
  // One way to add category (similar to how other properties are added)
pg->Append( new wxPropertyCategory("Main") );

  // Add int property
pg->Append( new wxIntProperty("IntProperty", wxPG_LABEL, 12345678) );
// Add float property (value type is actually double)
pg->Append( new wxFloatProperty("FloatProperty", wxPG_LABEL, 12345.678) );
// Add a bool property
pg->Append( new wxBoolProperty("BoolProperty", wxPG_LABEL, false) );
// A string property that can be edited in a separate editor dialog.
pg->Append( new wxLongStringProperty("LongStringProperty",
                                     wxPG_LABEL,
                                     "This is much longer string than the "
                                     "first one. Edit it by clicking the button."));

// One way to add category (similar to how other properties are added)
pg->Append( new wxPropertyCategory("Species") );

// String editor with dir selector button.
pg->Append( new wxDirProperty("DirProperty", wxPG_LABEL, ::wxGetUserHome()) );
// wxArrayStringProperty embeds a wxArrayString.
pg->Append( new wxArrayStringProperty("Label of ArrayStringProperty",
                                      "NameOfArrayStringProp"));
// A file selector property.
pg->Append( new wxFileProperty("FileProperty", wxPG_LABEL, wxEmptyString) );
// Extra: set wild card for file property (format same as in wxFileDialog).
pg->SetPropertyAttribute( "FileProperty",
                          wxPG_FILE_WILDCARD,
                          "All files (*.*)|*.*" );


// Iterate through PG, first serialization test ?
/* wxPropertyGridIterator it;

 cout << ">> wxPG iteration test: "<< endl;
 for ( it = pg->GetIterator();
      !it.AtEnd();
      it++ )
{
    wxPGProperty* p = *it;
    cout << "   - "<< p->GetLabel() << ": "<< p->GetValue().GetString() << endl;
}
*/
}



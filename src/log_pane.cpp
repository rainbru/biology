/* 
 * biology - A study to train a neural network to handle Non-Playable Characters
 *
 * Copyright (C) 2010, 2015-2018 Jérôme Pasquier
 *
 * biology is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * biology is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with biology.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "log_pane.h"

using namespace std;

LogPane::LogPane(TimeLine* tl,wxFrame *parent,int id,const wxPoint &pos,const wxSize &size):
  wxGrid(parent, id, pos, size),
  _timeline(tl)
{
  //       cols, rows
  CreateGrid(0, 3, wxGridSelectRows);
  SetColLabelValue (0, "Game date");
  SetColLabelValue (1, "Namespace");
  SetColLabelValue (2, "Message");

  SetColSize(0, 100);
  SetColSize(1, 100);
  SetColSize(2, 250);

log (LD_SIM, "The specy 'aze' is now dead");
}

LogPane::~LogPane()
{

}

string
LogPane::domainString(tLogDomain domain) const
{
  switch (domain)
    {
    case LD_GUI:
      return "Gui";
    case LD_SIM:
      return "Simulation";
    case LD_TEST:
      return "Test";
    default:
return "Unset";
    }
}

void
LogPane::log(tLogDomain domain, const string& message)
{
  AppendRows(1);
  int row = GetNumberRows() - 1;
  SetCellValue(row , 0, _timeline->gameDateTime());
  SetCellValue(row , 1, domainString(domain));
  SetCellValue(row , 2, message);
  MakeCellVisible(row, 0);
}


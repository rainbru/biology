/* 
 * biology - A study to train a neural network to handle Non-Playable Characters
 *
 * Copyright (C) 2010, 2015-2018 Jérôme Pasquier
 *
 * biology is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * biology is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with biology.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _LOG_PANE_H_
#define _LOG_PANE_H_

// For compilers that support precompilation, includes "wx/wx.h".
#include <wx/wxprec.h>
#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

#include <wx/grid.h>

#include <string>

#include "time_line.h"

typedef enum
{
  LD_GUI = 1,    // The graphical interface
  LD_SIM,        // the simulation
  LD_TEST        // Only for test purpose
}tLogDomain;

class LogPane: public wxGrid
{
 public:
  LogPane(TimeLine*, wxFrame*,int id,const wxPoint &pos,const wxSize &size);
  virtual ~LogPane();

  void log(tLogDomain, const std::string&);

 protected:
  std::string domainString(tLogDomain) const;

 private:
  TimeLine* _timeline; // Get it in constructor to get access to game date
};
  
#endif // !_LOG_PANE_H_

/* 
 * biology - A study to train a neural network to handle Non-Playable Characters
 *
 * Copyright (C) 2010, 2015-2018 Jérôme Pasquier
 *
 * biology is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * biology is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with biology.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "time_line.h"

enum
{
  TIMER_ID= 10,
  REFRESH_ID
};

wxBEGIN_EVENT_TABLE(TimeLine, wxPanel)
    EVT_TIMER(TIMER_ID,TimeLine::OnTimerTimeout)
    EVT_TIMER(REFRESH_ID,TimeLine::OnRefresh)
wxEND_EVENT_TABLE()

TimeLine::TimeLine(wxFrame *parent, int id, const wxPoint &pos,
		   const wxSize &size):
  wxPanel(parent, id, pos, size, wxSUNKEN_BORDER),
  m_parent(parent),
  m_timer(NULL),
  m_timer_refresh(NULL),
  m_time(NULL),
  m_incr_time(NULL),
  m_incr_date(NULL),
  timeout(200)
{
  // Here are the incremental values
  // Each time the interval is called, we add both these spans
  m_incr_time = new wxTimeSpan(0, 10, 0, 0); //Hour, Minutes, Sec. Millisec.
  m_incr_date = new wxDateSpan(0, 0, 1, 0); //Year, Month, Week, Day
  
  m_time = new wxDateTime(wxDateTime::UNow());
  wxLogDebug("Starting timer at %s", m_time->FormatISOCombined().c_str());

  m_timer = new wxTimer(this,TIMER_ID);
  m_timer->Start(timeout);

  m_timer_refresh = new wxTimer(this,REFRESH_ID);
  m_timer_refresh->Start(timeout);

  Connect(wxEVT_PAINT, wxPaintEventHandler(TimeLine::OnPaint));
  Connect(wxEVT_SIZE, wxSizeEventHandler(TimeLine::OnSize));
  
}

TimeLine::~TimeLine()
{

}

void
TimeLine::OnTimerTimeout(wxTimerEvent& event)
{
  m_time->Add(*m_incr_date);
  m_time->Add(*m_incr_time);
}

void
TimeLine::OnSize(wxSizeEvent& event)
{
  Refresh();
}

void
TimeLine::OnPaint(wxPaintEvent& event)
{
  wxFont font(9, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL,
	      wxFONTWEIGHT_NORMAL, false, wxT("Courier 10 Pitch"));

  wxPaintDC dc(this);
  dc.SetFont(font);
  wxSize size = GetSize();
  int width = size.GetWidth();
  int l = wxALIGN_LEFT|wxALIGN_TOP;
  int c = wxALIGN_CENTER|wxALIGN_TOP;
  wxBitmap n = wxBitmap();
  
  dc.DrawLabel("Game date:", n, wxRect(5, 5, 50, 15), l );
  dc.DrawLabel(gameDate(), n, wxRect(0, 15, width, 15), c );
  
  dc.DrawLabel("Game time:", n, wxRect(5, 35, 50, 15), l );
  dc.DrawLabel(gameTime(), n, wxRect(0, 45, width, 15), c );
}

void
TimeLine::OnRefresh(wxTimerEvent&)
{
  Refresh();
}

/** Return the game date in ISO format
  *
  */
wxString
TimeLine::gameDate()
{
  return m_time->FormatISODate();
}

/** Return the game time in ISO format
  *
  */
wxString
TimeLine::gameTime()
{
  return m_time->FormatISOTime();
}

/** Returns the current game date and game time as a string
  *
  * \return gameDate() + gameTime()
  *
  */
wxString
TimeLine::gameDateTime()
{
  wxString aze = gameDate();
  aze.append(" ");
  aze.append(gameTime());
  return aze;
}



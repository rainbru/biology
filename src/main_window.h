/* 
 * biology - A study to train a neural network to handle Non-Playable Characters
 *
 * Copyright (C) 2010, 2015-2018 Jérôme Pasquier
 *
 * biology is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * biology is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with biology.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _MAIN_WINDOW_H_
#define _MAIN_WINDOW_H_

// For compilers that support precompilation, includes "wx/wx.h".
#include <wx/wxprec.h>
#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

#include <wx/aui/aui.h>

// Forward declarations
class LogPane;
class PropertiesPane;
class NeuralNetwork;
// End of forward declarations


class MainWindow: public wxFrame
{
public:
  MainWindow(NeuralNetwork*,const wxString& title, const wxPoint& pos, const wxSize& size);
    virtual ~MainWindow();

 private:
    void OnHello(wxCommandEvent& event);
    void OnExit(wxCommandEvent& event);
    void OnAbout(wxCommandEvent& event);
    void OnNnConfiguration(wxCommandEvent& event);
 
  wxDECLARE_EVENT_TABLE();

    wxAuiManager m_mgr;
    LogPane* m_log;
    PropertiesPane* m_props;
    NeuralNetwork* m_neuralNetwork;
};

#endif // _MAIN_WINDOW_H_
